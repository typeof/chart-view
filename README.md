### 大屏数据可视化开发
- clone：

```shell
git clone https://gitee.com/typeof/chart-view.git
```

- install

```shell
cd chart-view 
npm install
```

- start

```shell
npm start
```


1. 大屏适配问题, 模块之前的大小，支持全屏事件
  提供设计稿 1920 * 1080
  - 第一种 vw vh 配合@media
  - rem 计算html的fontSize 
  - scale 缩放 控制根元素的缩放

2. 图表绘制
  背景 图表

3. http://datav-react.jiaminghi.com/guide/#%E7%94%A8%E5%89%8D%E5%BF%85%E7%9C%8B

data-v react版本

提供带边框的容器组件