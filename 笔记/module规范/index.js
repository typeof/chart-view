const reduceSum = (...arg) => {
  return arg.reduce((sum, cur) => sum + cur, 0)
}

export default reduceSum;