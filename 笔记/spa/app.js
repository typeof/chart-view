const http = require('http');
const fs = require('fs');

const server = http.createServer((req, res) => {
  console.log('requset url', req.url);
  if(req.url === '/' || req.url === '/my' || req.url === '/shop'){
    res.end(fs.readFileSync('./index.html'))
  }
})

server.listen(3000, () => {
  console.log('server port 3000')
})