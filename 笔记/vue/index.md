1. vue数据双向绑定的原理

监听数据的获取和设置

const data = {
  count: 0
}

Object.definedProperty()
// js中属性分成两大类
// 1. 数据属性
// 2. 访问器属性

`observer` 把data中所有的属性通过递归配合`Object.definedProperty`变成访问器属性，监听属性的get和set
`compiler` 指令模板解析 取template中所有以v-开始的属性，还有{{}}中间的模板内容
触发get函数获取数据值，添加`watcher(监听者)`到`Dep（队列）`中
触发set函数设置数据值，判断值是否更新，更新通知Dep队列找到所有watcher进行视图更新


2. v-model 指令的作用
v-model作用在表单元素上实现表单的元素双向绑定，它其实一个语法糖

<base-detail
  :theme="red"
  :requset="async (id) => {
    return {

    }
  }"
>
  <footer v-slot="footer">
  
  </footer>
</base-detail>

3. v-if 和 v-show的区别以及它的使用场景
v-if 控制元素的创建和销毁, 页面初始加载的loading，数据加载完成展示页面
v-show 控制元素的display:block/none  弹窗反复显示隐藏 按钮反复显示隐藏

4. 组件内data为什么是一个函数
保证组件状态是唯一的对象，保证组件状态互不影响
通过工厂模式来实现每次data函数返回新对象
const A = {
  data() {
    return {
      name: 'A'
    }
  }
}

5. vue的生命周期
8个生命周期 4个阶段
1. 创建阶段
  beforeCreate this没有创建完成，data和methods这些属性方法没有挂载到this上
  created 在当前生命周期拿到this，可以在this上挂载初始属性
2. 挂载阶段
  beforeMount 在挂载之前执行拿不到dom节点
  mounted 在当前生命周期拿到初始dom节点 做dom操作，可以绑定全局的一些时间
3. 更新阶段
  beforeUpdate
  updated 
4. 卸载阶段
  beforeDestory
  destoryed 卸载全局时间 和 定时器

6. 组件通信
- 父子组件通信
  props   this.$parent 获取父组件实例
- 子父组件通信
  $emit 自定义事件 this.$children 获取子组件实例
- 兄弟组件通信
  找父元素
  App
    A
    B
  发布订阅模式
- 跨组件通信
  provide在根组件或者父组件中提供状态  inject在子组件或者孙子组件用注入
- vuex
  在组件内通过dispatch派发action,获取异步操作数据，通过commit触发mutation，在
  mutaion中修改仓库状态，在组件内通过compute监听仓库状态变化，同步到组件视图

vue router 的两种模式
spa应用 单页面应用
  1. history 初次进入页面会有404问题，需要服务端单独配置
  2. hash