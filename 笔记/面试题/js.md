es5

1. 基本数据类型有什么

string 、 number、 boolean、 undefined、 null、 bigInt、 Symbol

object

2. 怎么判断数据类型

typeof value === 'object'

value instanceof String === true 

3. 怎么判断这个值是一个数组还是一个对象

Array.isArray(arr) 

Object.prototype.toString.call(value)  // '[object 类型]'

4. 写代码的时候哪些会进行类型转换
== 
if()
while()
!=

5. Number('123.12') parseInt()

6. isNumber('12+12')  isNumber('abc')  状态机 判断最后一个值是不是数字

7. Object.prototype指向什么
 首先解释prototype属性的作用，构造函数的原型对象
 再解释为什么会有原型对象？
  把一些公共的方法和属性放到原型对象上，这样在定义新对象时不会重复定义这些方法，可以节省
  内存开销。
 

8. 什么是构造函数？
 通过new关键字调用的函数叫构造函数
 通过new关键字调用函数干了几件事？
 1. 创建this对象 oa1
 2. 修改构造函数的this指向
 3. 运行该函数
 4. 返回this对象 oa1

9. 什么是原型链
 实例对象通过__proto__属性一直向上找直到找到null的一条链，叫原型链

10. 什么是继承 怎么实现继承
es6 实现继承 通过extends关键字来实现继承，配合super来调用父类方法
es5 实现继承 借用构造函数，实现属性和方法继承, 配合原型继承实现组合式继承

11. this指向
  当前运行环境的上下文对象
  this指向会随着函数调用方式发生改变

  1. 普通函数调用 
    严格模式下this指向undefined 非严格模式下this指向window
  2. 方法调用
    this指向拥有该方法的对象
  3. 隐式调用
    通过 call、apply方法来调用，this指向第一个参数 call apply 的参数不一样，call可以传递多个参数，apply第二个参数只能是一个数组
    bind方法返回一个绑定this指向的函数，需要手动调用
  4. 通过new关键字调用
    指向的实例对象
  5. 箭头函数， 箭头函数的this指向不会改变

12. 箭头函数和普通函数的区别

  - this指向不同，箭头函数的this取决于定义箭头函数的环境对象，而且不会改变
  - 普通函数的this指向会改变。
  - 普通函数可以当做构造函数来使用，箭头函数不可以
  - 普通函数可以使用arguments关键字来获取实参， 箭头函数不能使用arguments关键字
  - 箭头函数语法比较简洁，在直接有返回值的情况下，可以忽略return关键字

 ```
  function A(){

  }
  A();  // A 是普通函数
  new A() // A 是构造函数 A.prototype

  const arr1 = new Array(1,2,3);
  const arr2 = new Array(2,3,4);
  // Array.prototype.push
  arr1.push(4)
  arr2.push(6)
  arr1.push === arr2.push // true
 ```

es6
  1. let const

  - var 声明的变量会变量提升
  - let const 声明的变量不会提升，在声明之前不能做引用
  - var 声明的变量可以重复声明，但是let和const声明的变量不能重复声明
  - var 声明的变量不会产生块级作用域，let和const会
  - let const 会产生暂时性死区 
  - const 声明常量 let 声明变量
  ```js
  var a = 'a';
  var a = "a1"
  function a(){}
  console.log(a); // 'a'

  for(var i=0;i<5;i++){
    setTimeout(() => {
      console.log('var:i',i)
    })
  }
  for(let i=0;i<5;i++){
    setTimeout(() => {
      console.log('let:i',i)
    })
  }

  const obj = {};
  obj.name = "obj";


  const str = "hello word"
  function reduceStr(){
    console.log(str);
    let str = "test";
  }
  reduceStr()
  ```

  2. 箭头函数

  3. 解构赋值
  ```js
  const obj = {run:() => {}}
  const {run:runFn} = obj;
  ```
  4. 对象浅拷贝
  ```js
  const o = {
    name: 'o',
    age: 10,
    firends: [
      {
        name: "a",
        age: 11
      }
    ],
    sayname(){
      alert(this.name)
    }
  }
  const o1 = {...o};
  const o2 = Object.assign({}, o);
  ```
  5. 对象深拷贝
  ```js
  const o3 = JSON.parse(JSON.stringify(o));
  // 过滤掉函数 正则 时间日期

  // 自己实现深拷贝 递归
  const stack = [];
  function deepclone(value){
    // 数组 函数 对象 正则 时间日期 typeof value  object
    // 简单类型值
    if(typeof value === 'object'){
      if(!stack.includes(value)){
        stack.push(value);
        if(Array.isArray(value)){
          return value.map(val => deepclone(val))
        }
        if(typeof value === 'function'){
          return value;
        }
        return Object.keys(value).reduce((def, val) => {
          def[val] = deepclone(value[val]);
          return def;
        },{})
        } else {
          return value;
        }
      }else {
        return value;
      }
  }
  deepclone('11');
  deepclone(o);

  ```

  3. 根据key找value o.firends[0].name

  4. 在项目中怎么管理异步操作
  回调地狱
  ```js
    $.ajax({
      url: '/api/1',
      success(data){ // callback
        //处理别的逻辑
        $.ajax({
          url: '/api/2',
          success(data){ // callback
            //处理别的逻辑
            $.ajax({
              url: '/api/3',
              success(data){ // callback
                //处理别的逻辑
              }
            })
          }
        })
      }
    })
  ```
  Promise之后 链式调用
  Promise 容器 管理异步操作
  Promise有三种状态 pedding resolve reject

  ```js
  const ajax = ({url, method, data}) => {
    return new Promise((resolve, reject) => {
      const xhr = new XMLHttpRequset(); 
      xhr.open(method, url);
      xhr.send(data);
      xhr.onreadystatechange = function(){
        if(xhr.readystate === 4){ // 已经发送服务器
          if(xhr.status === 200){ // 服务器状态
            resolve(xhr.response)
            return;
          }
          reject(xhr.response);
        } else {
          console.log('loading...')
        }
      }
    })
  }
  ajax({
    url:'/api/1'
  })
  .then(res => {
    return ajax({
      url: '/api/2'
    })
  })
  .then(res => {
    return ajax({
      url: '/api/3'
    })
  })
  .then(res => {

  })
  ```

  10. eventloop  
  事件分为两个大类事件
  1. 宏任务： 定时器、事件、ajax 网络请求 浏览器内置任务
  2. 微任务： promise MutationObserver  es6新增的 

  ```
  console.log('start');
  new Promise((resolve) => {
    console.log('promise,函数')
    resolve('promise:success');
    setTimeout(() => {
      console.log('Promise,10秒之后执行')
    }, 10)
  }).then(res => { // 异步任务
    console.log(res);
  })
  setTimeout(() => {
    console.log('10秒之后执行')
  }, 10)
  console.log('end');
  ```
  11. Promise 原型方法 then catch finally
  then方法可以接受两个参数第一个是success时执行的回调函数
  第二个是fail时执行的回调函数

  catch方法用来捕获异步队列的错误函数，如果有then的第二个参数优先执行then第二个参数

  finally是所有状态都会执行的函数

  12. Promise的静态方法
    Promise.all Promise.allSettled Promise.race Promise.resolve Promise.reject
  手写封装Promiseall方法

  进入页面有20个接口请求同时发送，需要等所有接口完成之后去操作dom

  all 同时发起多个异步任务，只要有一个失败就结束任务返回失败状态

  allSettled 同时发起多个异步任务，会对所有的任务进行包装，失败也会返回成功
    [{
      status:'fulfilled',
      value: 成功结果
    }, {
      status:'rejected',
      reason: '失败信息'
    }]
  
  race 同时发起多个异步任务，只要有一个成功/失败，其他任务终止

  13. async await
  解决多个异步任务依赖关系的问题, 自动执行异步任务， 只要有一个任务失败函数就结束执行
  捕捉函数内部错误可以通过try catch语句
  async 函数默认返回值是一个promise对象

  14. js的模块发展
  通过script标签来管理js文件，有很多依赖关系，不容易维护
  ```html
  <script src="1.js"></script>
  <script src="2.js"></script>
  <script src="3.js"></script>
  ```
  AMD规范，requirejs提出，主要针对浏览器端。通过动态创建script标签来管理
  commonjs规范， nodejs出现之后提出commonjs规范，主要用在nodejs开发服务端应用不能使用在浏览器里
  ```js
  const fs = require('fs') // 引入
  module.exports = { // 抛出

  }
  ```
  esModule  es6提出的模块管理规范，统一服务端和浏览器端
  ```js
  import Vue from "vue"

  export default {};
  export const lib = 'lib';
  ```
  umd规范 webpack  通用模块定义规范(UniversalModuleDefinition)
  