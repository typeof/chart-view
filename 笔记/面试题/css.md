- 如何实现一个元素水平垂直居中？
  1. 给父元素添加弹性盒，设置xy轴居中
  ```css
  {
    display:flex;
    justify-content: center;
    align-items: center;
  }
  ```
  2. 通过定位来实现,设置绝对定位，设置left，top的距离为50%，通过transform translate位移实现向左向上偏移自身的50%，实现居中
  3. 通过定位给元素四个方位设置成0，设置margin：auto

- animation和transition的区别
  1. animation做关键帧动画，一进入页面就触发
  2. transition做过渡动画，需要事件触发

- 常见布局方式
flex布局
  怎么开启弹性盒： display:flex
  弹性盒默认的主轴方向：x轴 row flex-direction

- 常见单位
  px
  rem 配合 flex布局实现移动端适配
  em
  vw
  vh
  rpx 小程序


- 移动端常见兼容问题

 0.5px的问题
 {
  border-top:0.5px solid red;
 }
 ::before {
    content: "";
    position: absolute;
    top: 0;
    left: 0;
    height: 1px;
    width: 100%;
    background-color: red;
    transform: scaleY(0.5);
  }

 滚动穿透
  当有一个弹窗出现的并且弹窗可以滚动时，滚动弹窗会影响页面滚动条
  解决：弹窗出现时给页面元素添加overflow：hidden
       弹窗出现时阻止页面元素touchmove默认行为
 
 全面屏兼容
  通过css安全区域变量来适配全面屏

- 盒模型
  标准盒模型 content padding  border margin
  怪异盒模型
  box-sizing:border-box

- bfc规范
