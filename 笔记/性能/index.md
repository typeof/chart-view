1. 性能指标监控

谷歌浏览器控制台 查看性能指标
  performance 
    network 可以查看各个资源加载时间
    frame 文档判断加载时间
  FP  页面首次开始进行绘制的时间
  FCP 页面内容首次绘制的时间
  LCP 页面交互可以正常交互的时间
  DCL DocumentContentLoaded事件

  google 提供 lightHouse 工具可以产出性能分析报告
  google 提供 page speed 

在浏览器url地址栏输入地址发生什么？
1. 输入域名，解析ip地址 DNS(域名解析器)
2. 建立TCP链接 三次握手
3. 发送http请求 
4. 得到响应
5. 断开TCP 四次挥手
6. 浏览器渲染页面

2. 项目层面 常见性能优化的手段
  1. 代码层面
    减少http请求
      组件懒加载 import(() => A)
      分页
      图片懒加载
      防抖
      节流
      循环加 key
      精灵图
      图片分场景加载 
        小图标可以使用svg、base64  
        大图片png、jpg、webp
        列表加载缩略图 详情加载大图 图片地址后面加参数
  2. 风格约定
      eslint 代码检测
      preitter 代码风格约定
  3. vue 
    区分使用v-if和v-show
    组件懒加载
    ui组件库按需加载
    keep-alive
  4. react
    react.memo
    生命周期 shouldComponentUpdate 判断是不是要更新页面的属性变化
    useMemo
    useCallback
    循环加key
  
    
  2. 构建工具层面
    webpack
      1. 压缩js
      2. 压缩css
      3. 图片压缩 判断图片大小 小于50k变成base64 
      4. 设置文件别名  减少文件路径查找时间
      5. Externals 外部扩展实现第三方包的引入
      6. 区分环境 区分开发环境和生产环境配置 合理配置devTool


  3. web（服务端）层面
     CDN分布式网络 
     缓存
      强缓存  cache-control:max-age=10(s) / no-cache 
      协商缓存 
        强缓存失效走协商缓存
        服务器判断资源有没有变化？
        两种方式判断资源变化
          1. etag 文件版本号
          2. last-modified 文件最后一次修改时间
          有变化返回新资源和200状态码
          没有变化返回304状态码 结束响应



