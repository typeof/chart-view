function esDialog({title}){
  this.title = title;
  this.init();
}
esDialog.prototype.init = function(){
  this.createDialog();
}
esDialog.prototype.createDialog = function(){
  const div = this.dialogWrapper =  document.createElement('div');
  div.className = 'dialog';
  div.style.display = 'none';
  div.innerHTML = `<h1>${this.title}</h1>`;
  document.body.appendChild(div);
}
esDialog.prototype.show = function(){
  this.dialogWrapper.style.display = 'block';
}

new esDialog({
  title: 'es5弹窗'
});


function esDragDialog({title}){
  // this 指向esDragDialog的实例
  // es5继承 借用构造函数
  // 在子类的构造函数内去调用父类的构造函数
  esDialog.call(this, {title});
}
// 原型继承
esDragDialog.prototype = Object.create(esDialog.prototype);
esDragDialog.prototype.constructor = esDragDialog;
esDragDialog.prototype.init = function(){
  // 父类
  esDialog.prototype.init.call(this);
  this.drag();
}
esDragDialog.prototype.drag = function(){
  const dialog = this.dialogWrapper;
  dialog.style.position = 'absolute';
  dialog.addEventListener('mousedown', function(e){
    const disX = e.clientX - dialog.offsetLeft;
    const disY = e.clientY - dialog.offsetTop;
    document.addEventListener('mousemove', move);
    document.addEventListener('mouseup', up);
    function move(e){
      dialog.style.left = e.clientX - disX + 'px';
      dialog.style.top = e.clientY - disY + 'px';
    }
    function up(){
      document.removeEventListener('mousemove', move);
      document.removeEventListener('mouseup', up);
    }
  })
}

new esDragDialog({
  title: 'es5拖拽弹窗'
})