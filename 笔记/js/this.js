// 'use strict';  // 声明严格模式

// function run(nameValue){
//   console.log('run-this:', this, nameValue);
// }
// run(); // window 严格模式下为undefined

// var obj = {
//   name: 'obj',
//   run
// }
// obj.run(); // obj 方法调用 this指向拥有该方法的对象

// obj.run.call(null, 'null'); // null
// obj.run.call('', 'string'); // ''
// obj.run.apply(window, ['window']) // window

// obj.run.bind(window, 'bind-window')();


