const dialogBtn = document.querySelector('.dialogBtn');
const dialogDragBtn = document.querySelector('.dialogDragBtn');

class Dialog {
  constructor({title}){
    this.title = title;
    this.init();
  }
  init(){
    this.createDialog();
  }
  createDialog(){
    const div = this.dialogWrapper =  document.createElement('div');
    div.className = 'dialog';
    div.style.display = 'none';
    div.innerHTML = `<h1>${this.title}</h1>`;
    document.body.appendChild(div);
  }
  show(){
    this.dialogWrapper.style.display = 'block';
  }
}

const oDialog = new Dialog({
  title: '普通标题',
});

dialogBtn.addEventListener('click', function(){
  oDialog.show();
})

class DragDialog extends Dialog {
  constructor({title}){
    super({title});
  }
  init(){
    // 父类
    super.init();
    this.drag();
  }
  drag(){
    const dialog = this.dialogWrapper;
    dialog.style.position = 'absolute';
    dialog.addEventListener('mousedown', function(e){
      const disX = e.clientX - dialog.offsetLeft;
      const disY = e.clientY - dialog.offsetTop;
      document.addEventListener('mousemove', move);
      document.addEventListener('mouseup', up);
      function move(e){
        dialog.style.left = e.clientX - disX + 'px';
        dialog.style.top = e.clientY - disY + 'px';
      }
      function up(){
        document.removeEventListener('mousemove', move);
        document.removeEventListener('mouseup', up);
      }
    })
  }
  show(){
    super.show();
  }
}

const oDragDialog = new DragDialog({
  title: '拖拽弹窗',
});
dialogDragBtn.addEventListener('click', function(){
  oDragDialog.show();
})




