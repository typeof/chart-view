const _id = Symbol('id');
class Format {
  constructor() {
    this[_id] = 'format';
  }
}

const oA = new Format()


const n = BigInt('111111111111111111111111111111');
// const n2 = n + 10000000;
console.log(n);

const obj = {
  a: {
    b: {
      c: {
        d: 'd'
      }
    }
  }
}
function getValue(key, obj){
  const keys = key.split('.');
  let target = obj;
  for(let i = 0; i < keys.length; i++){
    target = target[keys[i]];
  }
  return target;
}

getValue('a.b.c.d', obj);
// console.log(obj.a.b.c.d)


