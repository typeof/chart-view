import BorderBox8 from '@jiaminghi/data-view-react/es/borderBox8';

const Block = ({children, width, height, ...colors}) => {
  return (
    <div style={{width: width ? width : '100%', height}}>
      <BorderBox8 {...colors}>
        {children}
      </BorderBox8>
    </div>
  )
}
export default Block;