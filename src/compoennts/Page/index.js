import Block from "../Block"
import Decoration8 from "@jiaminghi/data-view-react/es/decoration8";
import Charts from "@jiaminghi/data-view-react/es/charts"
import chart1Option from "./chart1"
const Page = () => {
  return (
    <div>
      <header className="header">
        <div className="headerTitle">
          <span>智慧物流服务中心</span>
          <Decoration8 style={{width: '300px', height: '40px'}} className="headerBg"></Decoration8>
          <Decoration8 reverse={true} style={{width: '300px', height: '40px'}}  className="headerBg"></Decoration8>
        </div>
      </header>
      <section className="section">
        <div>
          <Block height={350}>
            <Charts option={chart1Option} />
          </Block>
          <Block height={200}>图表</Block>
        </div>
        <div>
          <Block height={650}>图表</Block>
        </div>
        <div>
          <Block height={350}>图表</Block>
        </div>
      </section>   
          
    </div>
  )
  
}

export default Page;
