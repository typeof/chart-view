import './App.css'
import { useScale } from "./utils/setScale"
import { useState, useCallback, useLayoutEffect, useEffect } from "react"
import Loading from "@jiaminghi/data-view-react/es/loading"
import Page from "./compoennts/Page"
function App() {
  const [scale, setScale] = useState(1);
  const [loading, setLoading] = useState(true);
  const onResize = useCallback(() => {
    // eslint-disable-next-line react-hooks/rules-of-hooks
    const scale = useScale()
    setScale(scale);
  })
  useLayoutEffect(() => {
    // eslint-disable-next-line react-hooks/rules-of-hooks
    const scale = useScale()
    setScale(scale);
    window.addEventListener('resize', onResize);
    return () => {
      window.removeEventListener('resize', onResize);
    }
  }, [])
  useEffect(() => {
    setTimeout(() => {
      setLoading(false);
    }, 2000)
  }, [])
  return (
    <div className="App" style={{'transform': 'scale('+ scale +')'}}>
      {
        loading ? <Loading>Loading...</Loading> : <Page />
      }
      
    </div>
  );
}

export default App;
