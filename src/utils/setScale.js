const designWidth = 1920;
const designHeight = 1080;
export const useScale = () => {
  const clientWidth = window.innerWidth;
  const clientHeight = window.innerHeight;
  // 兼容全屏模式 不能出现滚动条
  const scale = Math.min(clientWidth / designWidth, clientHeight / designHeight);
  return scale;
}
